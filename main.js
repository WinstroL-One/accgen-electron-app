const {
  dialog,
  session,
  app,
  BrowserWindow,
  ipcMain,
  shell
} = require("electron");
const path = require("path");
let mainWindow;
const isDev = require("electron-is-dev");
var ps = require('ps-node');
var spawn = require('child_process').spawn;
var Registry = require('winreg');
const updater = require("./updater");

const filter = {
  urls: ['https://*.steampowered.com/*']
}

function createWindow() {
  mainWindow = new BrowserWindow({
    show: true,
    width: 1024,
    show: false,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      // Enables preload in iframes, node stuff is still off because of the variable above
      nodeIntegrationInSubFrames: true,
      backgroundColor: '#272B30',
      preload: path.join(__dirname, 'preload/preload.js'),
      enableRemoteModule: true,
      // Holy fuck turning this on will be a shitfest...
      contextIsolation: false
    }
  });

  mainWindow.webContents.on('new-window', function (e, url) {
    e.preventDefault();
    try {
      var parsed = new URL(url);
      if (parsed.protocol === "https:")
        shell.openExternal(url);
    } catch (error) {
      console.error(error)
    }
  });

  // This allegedly prevents error 101, but I have ZERO evidence of this myself. https://t.me/sag_bot_chat/93145
  mainWindow.webContents.userAgent = "Mozilla/5.0 (Macintosh; U; MacOS X 10_10_0; en-US; Valve Steam Client/1410811155; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.86 Safari/537.36";

  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
  mainWindow.loadURL("https://accgen.cathook.club");

  mainWindow.once('closed', function () {
    mainWindow = null;
  });
}

ipcMain.on("accgen-web-ready", (event, arg) => {
  console.log("Ready got!");
  if (!isDev)
    updater.default(event);
});

function stopSteam() {
  return new Promise(function (resolve) {
    ps.lookup({
      command: 'steam'
    }, function (err, resultList) {
      if (err) {
        throw new Error(err);
      }
      resultList = resultList.filter(function (entry) {
        var command = entry.command.toLowerCase()
        if (process.platform == "win32")
          return command.endsWith("steam.exe")
        else if (process.platform == "linux")
          return command.endsWith("ubuntu12_32/steam")
        else
          return command.endsWith("steam")
      });

      if (resultList.length > 0) {
        // Check if user allows us to kill other steams
        dialog.showMessageBox(null, {
          type: 'question',
          buttons: ['Yes', 'No'],
          defaultId: 2,
          title: 'Kill steam?',
          message: 'Steam is already running. Would you like to terminate steam and continue?'
        }, (response) => {
          console.log(response)
          var promises = [];
          if (response == 0) {
            for (var i = 0; i < resultList.length; i++) {
              if (resultList[i] && resultList[i].pid) {
                var pid = resultList[i].pid;
                promises.push(new Promise(function (resolve, reject) {
                  ps.kill(pid, {
                    timeout: 10
                  }, function (err) {
                    if (err) {
                      console.error(err);
                      resolve();
                    } else {
                      console.log('Process %s has been killed!', pid);
                      resolve();
                    }
                  });
                }));
              }
            }
          }
          Promise.all(promises).then(resolve);
        });
      } else
        resolve();
    });
  })
}

async function startSteam(account) {
  var path;
  if (process.platform == "win32") {
    regKey = new Registry({
      hive: Registry.HKCU,
      key: '\\Software\\Valve\\Steam\\'
    })
    await new Promise(function (resolve) {
      regKey.values(function (err, items) {
        if (err)
          path = err;
        else
          for (var i = 0; i < items.length; i++)
            if (items[i].name == "SteamExe") {
              path = items[i].value;
              resolve();
              break;
            }
        resolve();
      });
    })
  } else
    path = "steam";
  if (path instanceof Error || !path) {
    dialog.showMessageBox(null, {
      type: 'error',
      title: 'Failed to start steam',
      message: 'SAG Electron was not able to find the steam executable.',
      buttons: ['Ok']
    });
    return;
  }
  if (!account || !account.login || account.login.includes(" ") || account.login.includes("-") || !account.password || account.password.includes(" ") || account.password.includes("-")) {
    return;
  }
  var steam = spawn(path, `-login ${account.login} ${account.password}`.split(" "), {
    detached: true,
    stdio: 'ignore'
  });
  steam.on('error', function (err) {
    if (err.errno == "ENOENT")
      dialog.showMessageBox(null, {
        type: 'error',
        title: 'Failed to start steam',
        message: 'SAG Electron was not able to find the steam executable.',
        buttons: ['Ok']
      });
    console.error(err);
  });
}

ipcMain.on("start-steam", async (event, arg) => {
  await stopSteam();
  console.log(arg);
  startSteam(arg);
});

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('browser-window-created', function (e, window) {
  window.setMenuBarVisibility(false);
});

app.on('ready', function () {
  createWindow();
  setTimeout(() => {
    if (!updater.initialized && !isDev)
      updater.default(null);
  }, 10000);

  session.defaultSession.webRequest.onHeadersReceived(filter, (details, callback) => {
    if (!details.referrer)
      return callback({ cancel: false });
    var url = new URL(details.referrer);
    if (!url.hostname.endsWith("cathook.club"))
      return callback({ cancel: false });

    delete details.responseHeaders['X-Frame-Options'];
    details.responseHeaders['Access-Control-Allow-Origin'] = url.origin;
    details.responseHeaders['Access-Control-Allow-Credentials'] = "true";
    delete details.responseHeaders['Content-Security-Policy'];
    callback({
      cancel: false,
      responseHeaders: details.responseHeaders
    })
  });
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})
