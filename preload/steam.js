var actualCode = `
async function sendSolution() {
    var url = new URL(document.referrer);
    var output = {
        token: CaptchaText(),
        gid: $J('#captchagid').val()
    }
    window.parent.postMessage(JSON.stringify(output), url.hostname.endsWith("cathook.club") ? url.origin : "https://accgen.cathook.club"); //get recaptcha token and send to accgen
}

var sendSolutionInterval = setInterval(function() { if (CaptchaText()) { sendSolution(); clearInterval(sendSolutionInterval); }}, 500);
$J('#email').val('do_not_edit_this@do_not_change.this');
$J('#reenter_email').val('do_not_edit_this@do_not_change.this');
$J('#i_agree_check').prop('checked', true);
$J('body').removeClass();
// Hide stuff we don't need
$J('body').children().hide();
// Move captcha entry to the beginning of the page
$J("#captcha_entry").prependTo("body");
// Center it
$J("#captcha_entry").css({
    "width": "100%",
    "text-align": "center"
});
$J("#captcha_entry_recaptcha").css({
   "display": "inline-block"
});
`

function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

if (inIframe()) {
    window.onload = function () {
        var e = document.createElement("script");
        e.textContent = actualCode;
        (document.head || document.documentElement).appendChild(e);
    }
}
